------------------------constants--------------------------
charConst = 48
maxFieldCounter = 9
nonePrint = '?'
whitePrint = '_'
blackPrint = 'x'
unsolvablePuzzleMessage = "Mozaic can't be resolved"
typePuzzleName = "Enter puzzle file name"

----------------types definitions-------------------------
data FieldState = None | White | Black deriving (Eq, Show) --FieldState: field can be only white (non selected), black (selected) or none (we don't know now)
type FieldCounter = Maybe Int                              --FieldCounter: for fields with number it's number, for empty fields is Nothing
type Field = (FieldState, FieldCounter)                    --Field: each field has state and counter
type MosaicRow = [Field]                                   --MosaicRow: mosaic row is a list of fields in a row
type Mosaic = [MosaicRow]                                  --Mosaic: mosaic is a list of mosaic rows
type Triple a = (a, a, a)                                  --Triple: it's a stucture with 3 values, in our case it will be used to count black, white and none fields around field

-----------------------functions--------------------------
--creates unknown field
unknown :: FieldCounter -> Field
unknown = (,) None

--reads raw puzzle (from txt file)
readPuzzleRaw :: String -> IO [String]
readPuzzleRaw = fmap read <$> readFile

--parses raw puzzle (as chars) to mosaic represented by our structures
--function also adds white fields around whole mosaic to simplify calculations
--(then every field will have 8 neighbours, even those on edges)
parsePuzzle :: [String] -> Mosaic
parsePuzzle puzzle = addAdditionalFields $ (map . map) charToField puzzle

--converts char to field, if in raw puzzle field is represented by "."
--then return none field without number, otherwise try to change char number
--to int number
charToField :: Char -> Field
charToField '.' = (None, Nothing)
charToField char = unknown $ Just $ charToInt char

--converts field to char, we use it to print resolved mosaic
--each type of field has own representation (see constants)
fieldToChar :: Field -> Char
fieldToChar (None, Nothing) = nonePrint
fieldToChar (None, Just x) = toEnum $ x + charConst
fieldToChar (White, _) = whitePrint
fieldToChar (Black, _) = blackPrint

--converts char to int
charToInt :: Char -> Int
charToInt digit = subtract charConst $ fromEnum digit

--definition of additional field placed around mosaic
additionalField :: Field
additionalField = (White, Nothing)

--adds additional fields around mosaic
--it will add 2 columns and two rows
addAdditionalFields :: Mosaic -> Mosaic
addAdditionalFields x = extendedMosaic
  where
    size = lengthList (head x)
    additionalRow = replicate (size + 2) additionalField 
    extendedMosaic = wrap (map addAdditionalColumns x) additionalRow

--adds additional columns
addAdditionalColumns :: MosaicRow -> MosaicRow
addAdditionalColumns x = wrap x additionalField

--prints unsolved mosaic
printMosaic :: Mosaic -> IO ()
printMosaic print = mapM_ (putStrLn . map fieldToChar) print

--prints solved mosaic (or prints message if mosaic is unsolvable)
printResolvedMosaic :: Maybe Mosaic -> IO ()
printResolvedMosaic resolvedMosaic = do
  case resolvedMosaic of
    Just puzzle -> printMosaic puzzle
    Nothing -> putStrLn unsolvablePuzzleMessage

--checks if all fields of mosaic are solved
checkMosaic :: Mosaic -> Bool
checkMosaic [x] = checkRow x
checkMosaic (x:xs) = checkRow x && checkMosaic xs

--checks if all fields in row are resolved
checkRow :: [Field] -> Bool
checkRow [x] = checkField x
checkRow (x:xs) = checkField x && checkRow xs

--checks if field is resolved
--if field is black or white, it's resolved, if is none, it's not
checkField :: Field -> Bool
checkField (None, _) = False
checkField (_, _) = True

--helping function
--converts triple to list
tripleToList :: (a, a, a) -> [a]
tripleToList (a, b, c) = [a, b, c]

--helping function
--creates list of triplets from list
listToTriplets :: [a] -> [(a, a, a)]
listToTriplets list = zip3 list (drop 1 list) (drop 2 list)

--helping function
--creates list of squares (Triple with triplets)
listToSquares :: [[a]] -> [Triple (Triple a)]
listToSquares [a, b, c] = zip3 (listToTriplets a) (listToTriplets b) (listToTriplets c)

--checks of all fields are solved, which means all mosaic is complete
complete :: Mosaic -> Bool
complete = foldrSquares fieldCompleted True

--creates squares with values of none, black and white fields around field
foldrSquares :: ([[a]] -> b -> b) -> b -> [[a]] -> b 
foldrSquares f init b = foldr f init mosaicList
  where
    row = map tripleToList $ listToTriplets b
    mosaicTriplets = concatList $ map listToSquares row
    mosaicList = map (map tripleToList . tripleToList) mosaicTriplets

--checks if field is solved correctly
--field is resolved correctly when field number is not smaller than number 
--of black fields around, and difference between 9 and field number is not
--smaller than number of white fields around
fieldCompleted :: Mosaic -> Bool -> Bool
fieldCompleted _ False = False
fieldCompleted [_, [_, (_, Nothing), _], _] state = state
fieldCompleted field@[_, [_, (_, Just counter), _], _] _ = result
  where
    (none, white, black) = countFields $ concatList field
    blackCondition = black <= counter
    whiteCondition = white <= maxFieldCounter - counter
    result = blackCondition && whiteCondition

--helping function
--concats elements on list
concatList :: [[a]] -> [a]
concatList [x] = x
concatList (x:xs) = x ++ concatList xs

--helping function
--returns length of list
lengthList :: [a] -> Int
lengthList [] = 0
lengthList [x] = 1
lengthList (x:xs) = 1 + lengthList xs

--helping function
--adds element at the beginning and the end of a list
wrap :: [a] -> a -> [a]
wrap list element = element : list ++ [element]

--counts fields around field
countFields :: [Field] -> Triple Int
countFields = foldr (sumTriple . fieldStateAsTriple) (0, 0, 0)

--helping function
--adds two triplets
sumTriple :: Triple Int -> Triple Int -> Triple Int
sumTriple (a, b, c) (x, y, z) = (a+x, b+y, c+z)

--converts field state to triple
fieldStateAsTriple :: Field -> Triple Int
fieldStateAsTriple (None, _) = (1, 0, 0)
fieldStateAsTriple (White, _) = (0, 1, 0)
fieldStateAsTriple (Black, _) = (0, 0, 1)

--sets new field state
--we need to create new mosaic with new field
setFieldState :: Mosaic -> Int -> Int -> FieldState -> Mosaic
setFieldState mosaic x y state = take y mosaic ++ newRow ++ drop (y + 1) mosaic
  where
    (_, fieldCounter) = mosaic !! y !! x
    oldRow = mosaic !! y
    newRow = [take x oldRow ++ ((state, fieldCounter) : drop (x + 1) oldRow)]

--gets next field of mosaic (field's coordinates)
getNextField :: Int -> Int -> Mosaic -> (Int, Int)
getNextField x y mosaic | x + 1 == lengthList mosaic = (0, y + 1)
                        | otherwise = (x + 1, y)

--gets new mosaic depending on what state field has
--if field is none state we try to match if it should be with white or black field
--otherwise leave mosaic without changes
getNextMosaic :: Int -> Int -> Mosaic -> (Mosaic, Mosaic)
getNextMosaic x y mosaic = do
  case mosaic !! y !! x of
    (None, _) -> getMosaicPair x y mosaic
    (_, _) -> (mosaic, mosaic)

--gets pair of mosaic, where one mosaic have current field modified
--to white state, and other have black state
getMosaicPair :: Int -> Int -> Mosaic -> (Mosaic, Mosaic)
getMosaicPair x y mosaic = pair
    where
        white_mosaic = setFieldState mosaic x y White
        black_mosiac = setFieldState mosaic x y Black
        pair = (white_mosaic, black_mosiac)

--checks state of mosaic
--if mosaic is not complete, returns nothing
--else if every field is solved and it's correct - returns solved mosaic
--in other cases we try to solve mosaic
solve :: Int -> Int -> Mosaic -> Maybe Mosaic
solve x y mosaic
  | not $ complete mosaic = Nothing
  | checkMosaic mosaic && complete mosaic = Just mosaic
  | otherwise = solveOtherCases x y mosaic

--tries if mosaic is correct, if not, we return
--other mosaic given to function
trySolve :: Int -> Int -> Mosaic -> Mosaic -> Maybe Mosaic
trySolve x y mosaic_x mosaic_y = do
  case solve x y mosaic_x of
    Just mosaic -> Just mosaic
    Nothing -> solve x y mosaic_y

--solves case, when mosaic isn't solved
--it prepares 
solveOtherCases :: Int -> Int -> Mosaic -> Maybe Mosaic
solveOtherCases x y mosaic = solved_mosaic
    where
      (next_x, next_y) = getNextField x y mosaic
      (mosaic_white, mosaic_black) = getNextMosaic x y mosaic
      solved_mosaic = trySolve next_x next_y mosaic_white mosaic_black

-----------------------main function-----------------------------------
--run it to get solved puzze
--it reads puzzle from txt, prints it and solves it (starting from beginning)
--it prints solved puzzle
main :: IO ()
main = do
    putStrLn typePuzzleName
    path <- getLine
    raw <- readPuzzleRaw path
    let puzz = parsePuzzle raw
    printMosaic puzz
    let puzz_solved = solve 1 1 puzz
    printResolvedMosaic puzz_solved