import System.IO (readFile)

-- wczytaj łamigłówkę z pliku o podanej nazwie
readPuzzle :: String -> IO [String]
readPuzzle filename = do
    contents <- readFile filename -- odczytaj całą zawartość pliku
    let puzzle = read contents :: [String] -- utwórz listę napisów (zob. klasa typów Read)
    return puzzle

-- wczytywanie tablicy jako macierz cyfr
stringTableToTable :: [String] -> [[Int]]
stringTableToTable [] = []
stringTableToTable (x:xs) = [map charToInt x] ++ stringTableToTable xs

-- funkcja zamienia elementy tablicy na cyfry
charToInt :: Char -> Int
charToInt c | c == '.' = -1
            | otherwise = maybeIntToInt (digitToInt c)

-- funkcja pomocnicza: konwersja cyfry na inta
digitToInt :: Char -> Maybe Int
digitToInt c = lookup c (zip ['0'..'9'] [0..9])

-- funkcja pomocnicza: konwersja Maybe Int na Int
maybeIntToInt :: Maybe Int -> Int
maybeIntToInt mx = do
    case mx of
        Just x  -> x
        Nothing -> -1

main = do 
    puzzle <- readPuzzle "puzzle.txt" 
    putStrLn (show $ length $ puzzle) -- wyświetl liczbę wierszy łamigłówki
